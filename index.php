<!doctype html>  
<head>
<meta charset="UTF-8">
<title>The Hideout - BitBucket Repo Update</title>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<link rel="icon" href="images/favicon.gif" type="image/x-icon"/>
 <!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

<link rel="shortcut icon" href="images/favicon.gif" type="image/x-icon"/> 
<link rel="stylesheet" type="text/css" href="css/styles.css"/>
<script type="text/javascript">
var currentFormEle;
$(document).ready(function(){
    $('[data-action] button.btn').click(function(){
        thisForm = $(this).closest('[data-action]');
        currentFormEle = thisForm;
        pull = $(this).attr('name'), push = $(this).attr('name'), message = thisForm.find('#message').val(), pass = thisForm.find('input[name="pass"]').val();
        console.log("pull="+pull+"&push="+push+"&message="+message+"&pass="+pass);
        $.ajax({ type: "POST", url:push+'.php',
            data: "pull="+pull+"&push="+push+"&message="+message+"&pass="+pass,
            cache: true, success: function(results){
                console.log('shell is '+results);
                currentFormEle.find('pre').html(results);
            }
        });
    });
});

</script>
    </head>
    <body>
	
    <div id="container">
    <header>
 
    <a href="#" id="logo"><img src="images/logo.png" width="400" height="70" alt="logo"/></a>    
	</header>

   <div class="holder_content1">
    <section class="group4">
    <h3>BitBucket Repo Update</h3>
        Please press the button below to commit & push your changes to the BitBucket repository.<br/><br/>
        <div data-action="push">
            <pre></pre>
          Password:<br/>
          <input type="password" name="pass" class="style-1"><br/><br/>
          Commit description:<br/>
          <textarea id="message" name="message"></textarea><br/><br/>
          <button id="push" name="push" class="btn">Push changes to BitBucket</button>
        </div>
	</section>


       
    </div>
    </div>
   
   </body></html>
